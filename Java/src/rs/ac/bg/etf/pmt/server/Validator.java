package rs.ac.bg.etf.pmt.server;

public class Validator extends Handler {

	private String username;
	private String password;

	public Validator(Server server, String username, String password) {
		super(server);
		this.username = username;
		this.password = password;
	}

	@Override
	public void handleRequest() {
		server.area.append("User " + username + " successfully logged in!\n");
		server.area.invalidate();
		Server.l(username + "authenticated!");
	}

}
