package rs.ac.bg.etf.pmt.server;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.*;

import rs.ac.bg.etf.pmt.server.exceptions.FileSendingException;

/**
 * Represents main Server and its GUI, through which all connections are
 * established and managed
 */

public class Server extends JFrame implements Runnable {
	private static final long serialVersionUID = 1L;
	static final int MAX_AVAIL_CONNECTIONS = 40;
	static final String BL_USERS_FILE = "blacklist_users.txt";
	static final String BL_IP_ADDR_FILE = "blacklist_ip_addr.txt";
	static final int connectPort = 16000;
	protected String Name = "Server";
	protected String devices_text = "";
	JButton send, exit;
	JTextArea area;
	JTextArea devices;
	JTextField box;
	ServerSocket ss;
	Socket client;
	BufferedReader in;
	PrintWriter out;
	ArrayList<Connection> connections;
	Thread thread;
	int freePorts = MAX_AVAIL_CONNECTIONS;
	static File log;
	static File bl_users = new File(BL_USERS_FILE);
	static File bl_addr = new File(BL_IP_ADDR_FILE);
	static PrintWriter logStream = null;
	CredentialManager cred = new CredentialManager();

	/**
	 * Default constructor which creates main GUI for Server application, and
	 * manages all connections
	 */
	public Server() {
		super("Chat server v1.1");
		ImageIcon img = new ImageIcon("res/server.png");
		setIconImage(img.getImage());
		createLog();
		fillWindow();
		setVisible(true);
		setSize(800, 600);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					disconnect();
				} catch (IOException ex) {
				}
				dispose();
			}
		});
		try {
			ss = new ServerSocket(connectPort);
		} catch (IOException ex) {
			l("Failed to establish server! Exiting...");
			System.exit(1);
		}
		thread = new Thread(this);
		thread.start();
		l("Server started!\n");
	}

	/**
	 * Fills main window with all components
	 */
	private void fillWindow() {
		connections = new ArrayList<>();
		area = new JTextArea();
		box = new JTextField(40);
		JPanel pan = new JPanel(new BorderLayout(0, 1));
		JPanel panel = new JPanel(new BorderLayout(0, 1));
		JPanel con_pan = new JPanel(new BorderLayout(0, 1));

		JPanel tmp_p = new JPanel();
		JLabel lab1 = new JLabel();
		lab1.setIcon(new ImageIcon("res/devices.png"));
		tmp_p.add(lab1);
		lab1 = new JLabel("Connected devices");
		tmp_p.add(lab1);
		con_pan.add(tmp_p, "North");
		devices = new JTextArea();
		devices.setFont(new Font(Font.SANS_SERIF, 0, 14));
		devices.setForeground(Color.BLACK);
		devices.setEditable(false);
		con_pan.add(new JScrollPane(devices), "Center");

		pan.add(new JLabel("Chat:"), "North");
		JScrollPane pane = new JScrollPane(area);
		pane.getVerticalScrollBar().addAdjustmentListener(
				new AdjustmentListener() {
					public void adjustmentValueChanged(AdjustmentEvent e) {
						e.getAdjustable().setValue(
								e.getAdjustable().getMaximum());
					}
				});
		area.setFont(new Font(Font.SANS_SERIF, 0, 14));
		area.setForeground(Color.BLACK);
		pan.add(pane, "Center");
		pan.add(con_pan, "East");
		panel.add(pan, "Center");
		pan = new JPanel();
		pan.add(new JLabel("Your message: "));
		pan.add(box);
		panel.add(pan, "South");
		add(panel, "Center");
		panel = new JPanel(new FlowLayout());
		area.setEditable(false);

		send = new JButton("Send message");
		exit = new JButton("Exit program");

		send.addActionListener(new Sender(this));
		exit.addActionListener(new Exiter(this));

		getRootPane().setDefaultButton(send);
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("File");
		JMenuItem item = new JMenuItem("Change server name", new ImageIcon(
				"res/rename.png"));
		KeyStroke f2 = KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0);
		item.setAccelerator(f2);
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String input = JOptionPane.showInputDialog("New name: ");
				if (input != null) {
					Name = input;
					if (Name == "")
						Name = "Server";
					l("Server name changed to " + Name);
				}
			}
		});
		menu.add(item);
		item = new JMenuItem("Open log file", new ImageIcon("res/log.png"));
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				createLog();
				if (log == null) {
					JOptionPane.showMessageDialog(Server.this,
							"Log not created", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				Desktop dt = Desktop.getDesktop();
				try {
					dt.open(log);
				} catch (Exception e1) {
				}
			}

		});
		KeyStroke f3 = KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0);
		item.setAccelerator(f3);
		menu.addSeparator();
		menu.add(item);
		item = new JMenuItem("Delete log file", new ImageIcon("res/delete.png"));
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (log == null) {
					JOptionPane.showMessageDialog(Server.this,
							"Log not created", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				int result = JOptionPane.showConfirmDialog(Server.this,
						"Delete log file?", "Confirm",
						JOptionPane.OK_CANCEL_OPTION);
				if (result == JOptionPane.OK_OPTION) {
					if (logStream != null)
						logStream.close();
					boolean ret = log.delete();
					log = null;
					logStream = null;
					if (ret == false) {
						JOptionPane.showMessageDialog(Server.this,
								"Can't delete log!", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}

		});
		KeyStroke f4 = KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0);
		item.setAccelerator(f4);
		menu.add(item);
		menu.addSeparator();
		item = new JMenuItem("List credential database", new ImageIcon(
				"res/database.png"));
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final JPanel window = new JPanel(new BorderLayout());
				JList<String> list = new JList<String>(cred.listDatabase());
				list.setFont(new Font(Font.SANS_SERIF, 0, 12));
				window.add(list, "Center");
				JOptionPane.showMessageDialog(Server.this, window,
						"Credentials", JOptionPane.INFORMATION_MESSAGE);
			}

		});
		KeyStroke f5 = KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0);
		item.setAccelerator(f5);
		menu.add(item);
		item = new JMenuItem("Edit credential in database");
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final JPanel window = new JPanel(new BorderLayout());
				JPanel panel = new JPanel(new GridLayout(0, 1));
				panel.add(new JLabel(
						"Enter either username or password and check what part is not being changed."));
				panel.add(new JLabel(
						"WARNING: If nothing selected whole credential is being deleted!"));
				JPanel tmp_p = new JPanel();
				JCheckBox cb_u = new JCheckBox();
				JLabel lab = new JLabel("Username");
				JTextArea ed_t1 = new JTextArea(1, 20);
				tmp_p.add(cb_u);
				tmp_p.add(lab);
				tmp_p.add(ed_t1);
				panel.add(tmp_p);
				tmp_p = new JPanel();
				JCheckBox cb_p = new JCheckBox();
				lab = new JLabel("Password");
				JTextArea ed_t2 = new JTextArea(1, 20);
				tmp_p.add(cb_p);
				tmp_p.add(lab);
				tmp_p.add(ed_t2);
				panel.add(tmp_p);
				window.add(panel);
				JOptionPane.showMessageDialog(Server.this, window,
						"Credentials", JOptionPane.OK_CANCEL_OPTION
								| JOptionPane.WARNING_MESSAGE);
			}

		});
		KeyStroke f6 = KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0);
		item.setAccelerator(f6);
		menu.add(item);
		menu.addSeparator();

		item = new JMenuItem("Blacklisted useres");
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Desktop dt = Desktop.getDesktop();
				try {
					dt.open(bl_users);
				} catch (Exception e1) {
					try {
						new PrintWriter(bl_users);
						dt.open(bl_users);
					} catch (IOException e2) {
						l("Failed to create blacklist file!");
					}
				}
			}

		});
		KeyStroke f7 = KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0);
		item.setAccelerator(f7);
		menu.add(item);

		item = new JMenuItem("Blacklisted IP addresses");
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Desktop dt = Desktop.getDesktop();
				try {
					dt.open(bl_addr);
				} catch (Exception e1) {
					try {
						new PrintWriter(bl_addr);
						dt.open(bl_addr);
					} catch (IOException e2) {
						l("Failed to create blacklist file!");
					}
				}
			}

		});
		KeyStroke f8 = KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0);
		item.setAccelerator(f8);
		menu.add(item);

		item = new JMenuItem("Add entry to blacklist", new ImageIcon(
				"res/blacklist.gif"));
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String input = JOptionPane
						.showInputDialog("Enter username or IP address that you want banned:");
				if (input != null) {
					try {
						InetAddress.getByName(input);
						try {
							FileWriter wr = new FileWriter(BL_IP_ADDR_FILE,
									true);
							wr.write(input
									+ System.getProperty("line.separator"));
							wr.flush();
							wr.close();
						} catch (Exception e1) {
							l("Unable to open blacklist file for writing!");
						}
					} catch (UnknownHostException exp) {
						try {
							FileWriter wr = new FileWriter(BL_USERS_FILE, true);
							wr.write(input
									+ System.getProperty("line.separator"));
							wr.flush();
							wr.close();
						} catch (Exception e1) {
							l("Unable to open blacklist file for writing!");
						}
					}
				}
			}

		});
		KeyStroke f9 = KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0);
		item.setAccelerator(f9);
		menu.add(item);
		menu.addSeparator();

		item = new JMenuItem("Register user");
		item.addActionListener(new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final JPanel window = new JPanel(new GridLayout(0, 1));
				JPanel tmp = new JPanel();
				tmp.add(new JLabel("Username: "));
				JTextField username = new JTextField(20);
				tmp.add(username);
				window.add(tmp);
				tmp = new JPanel();
				tmp.add(new JLabel("Password: "));
				JPasswordField password = new JPasswordField(20);
				tmp.add(password);
				window.add(tmp);
				int result = JOptionPane.showConfirmDialog(Server.this, window,
						"Register", JOptionPane.OK_CANCEL_OPTION
								| JOptionPane.WARNING_MESSAGE);
				if (result == JOptionPane.OK_OPTION) {
					cred.insert(username.getText(), password.getText());
				}
			}

		});
		KeyStroke f10 = KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0);
		item.setAccelerator(f10);
		menu.add(item);
		item = new JMenuItem("Delete username/password from database");
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				final JPanel window = new JPanel(new BorderLayout(0, 1));
				final JList<String> list = new JList<String>(cred
						.listDatabase());
				window.add(list, "Center");
				JButton button = new JButton("Delete entry");
				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						cred.removeEntry(Integer.parseInt(list
								.getSelectedValue().charAt(0) + ""));
						DefaultListModel<String> model = new DefaultListModel<String>();
						String[] data = cred.listDatabase();
						for (String str : data)
							model.addElement(str);
						list.setModel(model);
					}

				});
				window.add(button, "South");
				JOptionPane.showMessageDialog(Server.this, window,
						"Delete from database", JOptionPane.OK_OPTION
								| JOptionPane.WARNING_MESSAGE);
			}

		});
		KeyStroke f11 = KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0);
		item.setAccelerator(f11);
		menu.add(item);
		menu.addSeparator();

		item = new JMenuItem("Exit", new ImageIcon("res/exit.gif"));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					disconnect();
				} catch (Exception ex) {
				}
				dispose();
			}
		});
		menu.add(item);
		menubar.add(menu);

		menu = new JMenu("Help");
		item = new JMenuItem("Help", new ImageIcon("res/help.png"));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Server.openWebpage(new URL("http://rti.etf.bg.ac.rs"));
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
			}
		});
		KeyStroke f1 = KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0);
		item.setAccelerator(f1);
		menu.add(item);

		item = new JMenuItem("About", new ImageIcon("res/about.gif"));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JPanel window = new JPanel(new BorderLayout());
				JTextArea list = new JTextArea();
				list.setFont(new Font(Font.SANS_SERIF, 0, 14));
				list.setText("Author:\n\n"
						+ "Dejan Marković, mail: bodemarkovic@gmail.com\n\n"
						+ "This is part of initial testing stil.\n"
						+ "report any bugs you notice to author.\n");
				list.setEditable(false);
				window.add(list, "Center");
				JOptionPane.showMessageDialog(Server.this, window, "About",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		menu.add(item);
		menubar.add(menu);
		setJMenuBar(menubar);

		panel.add(send);
		panel.add(exit);
		add(panel, "South");
	}

	/**
	 * Run method for Server in which Server accepts external connections
	 */
	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				client = ss.accept();
				in = new BufferedReader(new InputStreamReader(
						client.getInputStream()));
				out = new PrintWriter(client.getOutputStream(), true);
				String message = in.readLine();
				if (message.equals("#connect request#")) {

					freePorts--;
					if (freePorts >= 0) {
						if (!devices_text.contains(client.getInetAddress()
								.toString() + "\n")) {
							devices_text += client.getInetAddress().toString()
									+ "\n";
						}
						devices.setText(devices_text);
						addConnection(new Connection(this, in, out, client));
						out.write("#connected#\n");
						out.flush();
						String str = client.getInetAddress().toString();
						messageConnected(str);
					} else {
						freePorts++;
						out.write("#refused#No more available ports!#\n");
					}

				} else {
					JOptionPane.showMessageDialog(null,
							"Unknown connection detected! Dropping...",
							"Warning", JOptionPane.WARNING_MESSAGE);
				}

				/*
				 * if (in != null) { in.close(); } if (out != null) {
				 * out.close(); } if (client != null) { client.close(); }
				 */
			}
		} catch (Exception e) {
			if (client != null) {
				try {
					if (in != null) {
						in.close();
					}
					if (out != null) {
						out.close();
					}
					client.close();
				} catch (IOException ex) {
				}
			}
		}
	}

	/**
	 * finalize methhod overriden to leave network connections in consistent
	 * state after exiting application
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		disconnect();
	}

	/**
	 * Disconnecting everything from server
	 * 
	 * @throws IOException
	 */
	void disconnect() throws IOException {
		l("Server closing...\n");
		if (logStream != null) {
			logStream.close();
		}
		if (in != null) {
			in.close();
		}
		if (out != null) {
			out.close();
		}
		if (client != null) {
			client.close();
		}
		if (ss != null) {
			ss.close();
		}
		int size = connections.size();
		for (int i = 0; i < size; i++) {
			try {
				Connection c = connections.remove(i);
				if (c != null)
					c.interrupt();
				if (c != null)
					c.disconnect();
				freePorts++;
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Info message for received connection
	 * 
	 * @param toString
	 *            IPAddress of user Client who connected to server
	 */
	synchronized void messageConnected(String toString) {
		area.append("*** Clent from " + toString + " connected to server!***\n");
	}

	/**
	 * Info message when disconecting client
	 * 
	 * @param toString
	 *            IPAddress of user Client who disconnected from server
	 */
	synchronized void messageDisconnected(String toString) {
		area.append("*** Clent from " + toString
				+ " disconnected from server!***\n");
	}

	/**
	 * After accepting new connection this method starts new Thread for managing
	 * it
	 * 
	 * @param c
	 *            Connection that has been established is stored in structure
	 *            inside Server
	 */
	synchronized void addConnection(Connection c) {
		c.start();
		connections.add(c);
	}

	public static void main(String[] args) {
		new Server();
	}

	/**
	 * Changing Server name
	 */
	public synchronized void setUsername(String name) {
		Name = name;
	}

	/**
	 * Return current name of Server
	 */
	@Override
	public String getName() {
		return Name;
	}

	/**
	 * Internal methond to send message to all connections (and on main GUI of
	 * Server itself)
	 * 
	 * @param data
	 *            Message being sent
	 * @param c
	 *            Connection which sent message (null if it is Server itself)
	 */
	synchronized void sendToAll(String data, Connection c) {
		int size = connections.size();
		for (int i = 0; i < size; i++) {
			Connection con = connections.get(i);
			if (c != con) {
				con.sendMessage(data);
			}
		}
		l(data);
		if (c != null) {
			area.append(data);
		}
	}

	/**
	 * Help method for forced shutdown of connections
	 * 
	 * @param c
	 *            Connection to be removed (disconnected)
	 */
	synchronized void remove(Connection c) {
		connections.remove(c);
		freePorts++;
	}

	/**
	 * Help method for launching web page
	 * 
	 * @param uri
	 *            uri to open
	 */
	public static void openWebpage(URI uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop()
				: null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Launching web page
	 * 
	 * @param url
	 *            url to be open
	 */
	public static void openWebpage(URL url) {
		try {
			openWebpage(url.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds log info passed as argument to log file (if it exists)
	 * 
	 * @param data
	 *            log to be written
	 */
	static void l(String data) {
		createLog();
		if (log == null)
			return;
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss")
				.format(Calendar.getInstance().getTime());
		String nl = System.getProperty("line.separator");
		logStream.write(timeStamp + " # " + data + nl);
		logStream.flush();
	}

	/**
	 * Creates log file
	 */
	private static void createLog() {
		if (log == null) {
			log = new File("ServerLog.txt");
			try {
				logStream = new PrintWriter(new FileWriter(log, true), true);
			} catch (IOException e1) {
				log = null;
				logStream = null;
				return;
			}
		}
	}

	/**
	 * Remove entry from list of active devices in chat conversation.
	 * 
	 * @param target
	 *            name present in list
	 */
	public void removeFromDevices(String target) {
		devices_text = devices_text.replace(target + "\n", "");
		devices.setText(devices_text);
	}

	/**
	 * Check if supplied user name exists among current users
	 * 
	 * @param username
	 *            String containing name to be checked
	 * @return boolean representing state
	 */
	public boolean usernameExists(String username) {
		for (Connection c : connections) {
			if (c.getUsername().equals(username))
				return true;
		}
		for (Connection c : connections) {
			if (c.getIPAddressOfConnection().equals(username))
				return true;
		}
		return false;
	}

	/**
	 * Send specified file to this user
	 * 
	 * @param filename
	 *            Name of file that is being sent to user
	 * @param size
	 *            Size of file being sent
	 * @param file
	 * @throws FileSendingException
	 *             thrown if error happens while sending file
	 */
	public void sendToUsername(String sender, String dest, String filename, int size,
			File file) throws FileSendingException {
		try {
			for (Connection c : connections) {
				if(c.getUsername().equals(dest))c.sendFile(filename, size, file);
			}
		} catch (Exception e) {
			throw new FileSendingException();
		}
	}

}
