package rs.ac.bg.etf.pmt.server;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import rs.ac.bg.etf.pmt.server.exceptions.*;

;

/**
 * Represents one Client connected to Server
 */
public class Connection extends Thread {

	private static final int MAX_FILE_SIZE = 10 * 1024 * 1024;
	private Socket s;
	private BufferedReader in;
	private PrintWriter out;
	private Server server;
	private String name = "Unknown";
	private String ip_address;

	public Connection(Server server, BufferedReader in, PrintWriter out,
			Socket s) {
		this.server = server;
		this.out = out;
		this.in = in;
		this.s = s;
	}

	/**
	 * Managing receiving messages from client
	 */
	@Override
	public void run() {
		try {
			ip_address = s.getInetAddress().toString().substring(1);
			name = ip_address;
			Server.l(ip_address + " accepted connection!\n");
			while (!Thread.interrupted()) {
				receiveMessage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (in == null || s == null || out == null) {
			server.remove(this);
			disconnect();
		}
	}

	/**
	 * Receive message from client
	 */
	void receiveMessage() {
		String message = null;
		try {
			message = in.readLine();
		} catch (IOException ex) {
			server.remove(this);
			disconnect();
		}
		if (message != null) {

			// end of connection
			if (message.equals("#END#")) {
				Server.l(s.getInetAddress().toString()
						+ " closed connection!\n");
				server.remove(this);
				server.removeFromDevices(s.getInetAddress().toString());
				server.messageDisconnected(s.getInetAddress().toString());
				disconnect();

			} else if (message.length() > 7
					&& message.substring(0, 7).equals("#LOGIN#")) {

				// login request from client
				Server.l("login attempt from " + s.getInetAddress().toString());
				StringTokenizer tk = new StringTokenizer(message, "#");
				tk.nextToken();
				String username = tk.nextToken();
				String password = tk.nextToken();

				// check if validation success
				if (server.cred.validate(username, password)) {
					name = username;
					new Validator(server, username, password).handleRequest();
					out.print("#AUTHENTICATED#\n");
				} else {
					name = "Unknown";
					out.print("Invalid username and/or password!\n");
					Server.l(name + " not authenticated!");
				}
				out.flush();
			} else if (message.equals("#SEND_FILE#")) {

				try {
					String filename = in.readLine();
					int filesize = Integer.parseInt(in.readLine());
					String destination_username = in.readLine();
					byte[] buffer = new byte[filesize];

					if (filesize > MAX_FILE_SIZE)
						throw new FileSizeException((name.equals("Unknown") ? s
								.getInetAddress().toString() : name));

					if (server.usernameExists(destination_username)) {

						File recvdFile = new File(filename);

						// delete if already exists
						if (recvdFile.exists()) {
							recvdFile.delete();
						}
						FileOutputStream fout = new FileOutputStream(recvdFile);

						InputStream input = s.getInputStream();

						int len = 0, curr_size = 0;
						while (curr_size < filesize
								&& (len = input.read(buffer)) != -1) {
							fout.write(buffer, 0, len);
							curr_size += len;
						}

						fout.close();
						System.out.println("File received: "
								+ recvdFile.length());

						try {
							server.sendToUsername(name, destination_username,
									filename, filesize, recvdFile);
						} finally {
							recvdFile.delete();
						}

						sendMessage("File sent to specified username!\n");

					} else {
						throw new UnknownUsernameException(name,
								destination_username);
					}

				} catch (Exception e) {
					// change output
					JOptionPane.showMessageDialog(server,
							"Fatal error while receiving file from client!\n"
									+ e, "Error", JOptionPane.ERROR_MESSAGE);
					Server.l(e.getMessage());
					sendMessage("ERROR: Can not send file!\n");
				}

			} else {

				new Messager(server, name, message, s.getInetAddress()
						.toString(), this).handleRequest();
			}
		}
	}

	/**
	 * Send message from Server to client
	 * 
	 * @param data
	 *            message that is being sent
	 */
	void sendMessage(String data) {
		if (data == null || out == null || data.equals("Server:")) {
			return;
		}
		out.write((data + "\n"));
		out.flush();
	}

	/**
	 * Closing all streams and network connections to the Client
	 */
	void disconnect() {
		try {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			if (s != null) {
				s.close();
			}
		} catch (IOException ex) {
		}
	}

	/**
	 * Called to leave everything in consistent state (streams, connections ...)
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		disconnect();
	}

	/**
	 * String info about this connection to Client
	 */
	@Override
	public String toString() {
		StringBuilder tmp = new StringBuilder();
		if (s != null) {
			tmp.append(name);
			tmp.append(" - ");
			tmp.append(s.getInetAddress().toString().substring(1));
			tmp.append("\n");
		} else
			return "dropped";
		return tmp.toString();

	}

	/**
	 * Returns name of user on the other end of connection.
	 */
	public String getUsername() {
		return name;
	}

	/**
	 * Send file to end user
	 * 
	 * @param filename
	 *            name of file to be sent from server
	 * @param file
	 */
	public void sendFile(String filename, int size, File file) {

		try {
			out.write("#RECEIVE_FILE#\n");
			out.flush();
			out.write("#" + filename + "#" + size + "#" + name + "#\n");
			out.flush();

			FileInputStream fin = new FileInputStream(file);
			InputStreamReader inF = new InputStreamReader(fin);

			int len = 0, curr_size = 0;
			byte[] buffer = new byte[size];
			
			OutputStream o = s.getOutputStream();
			
			while (curr_size < size
					&& (len = fin.read(buffer)) != -1) {
				o.write(buffer, 0, len);
				curr_size += len;
			}
			o.flush();

			inF.close();

		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public String getIPAddressOfConnection() {
		return ip_address;
	}

}
