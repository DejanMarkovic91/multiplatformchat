package rs.ac.bg.etf.pmt.server.exceptions;

public class FileSizeException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Thrown when user tries to send too big file to other user.
	 * 
	 * @param username
	 *            Name of user that attempted this illegal action
	 */
	public FileSizeException(String username) {
		super("File received from user " + username + " is too large!");
	}

}
