package rs.ac.bg.etf.pmt.server.exceptions;

public class FileSendingException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Thrown if server fails to reach destination user while transfering file.
	 */
	public FileSendingException(){
		super("Failed to send file to specified username!");
	}
	
}
