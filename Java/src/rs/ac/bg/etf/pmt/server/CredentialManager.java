package rs.ac.bg.etf.pmt.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CredentialManager {
	private String dbURL = "jdbc:derby:myDB;create=true;";
	private static String tableName = "credentials";
	private Connection conn = null;
	private Statement stmt = null;

	public CredentialManager() {
		createConnection();
		if (!tableExists()) {
			createTable();
		}
		numOfEntries();
	}

	private void createConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
			conn = DriverManager.getConnection(dbURL);
		} catch (Exception e) {
			Server.l(e.getMessage());
		}
	}

	private boolean tableExists() {
		try {
			stmt = conn.createStatement();
			stmt.execute("SELECT COUNT(*) from " + tableName);
			stmt.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void createTable() {
		try {
			stmt = conn.createStatement();
			try {
				stmt.execute("DROP table " + tableName);
			} catch (Exception e) {
			}
			stmt.execute("CREATE table "
					+ tableName
					+ "(id INTEGER PRIMARY KEY ,USERNAME VARCHAR(30), PASSWORD VARCHAR(30))");
			stmt.close();
		} catch (Exception e) {
			Server.l(e.getMessage());
		}
	}

	public synchronized void insert(String username, String password) {
		int cur_id = numOfEntries();
		try {
			stmt = conn.createStatement();
			stmt.execute("insert into " + tableName + " values ("
					+ (cur_id + 1) + ",'" + username + "','" + password + "')");
			stmt.close();
		} catch (SQLException sqlExcept) {

		}
	}

	public synchronized void removeEntry(int selectedIndex) {
		try {
			stmt = conn.createStatement();
			stmt.execute("delete from " + tableName + " where id = "
					+ selectedIndex);
			stmt.close();
		} catch (SQLException sqlExcept) {
			sqlExcept.printStackTrace();
		}
	}

	public synchronized int numOfEntries() {
		int num = 0;
		try {
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery("SELECT COUNT(*) FROM "
					+ tableName);
			results.next();
			num = results.getInt(1);
			stmt.close();
		} catch (SQLException e) {
			num = 0;
		} catch (Exception e) {
			Server.l(e.getMessage());
		}
		Server.l("Number of entries in database: " + num);
		return num;
	}

	public synchronized boolean validate(String username, String password) {
		try {
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery("SELECT * FROM " + tableName
					+ " WHERE username='" + username + "' AND password='"
					+ password + "'");
			results.next();
			results.getInt(1);
			stmt.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	protected synchronized String[] listDatabase() {
		String[] st = new String[numOfEntries()];
		int curInd = 0;
		try {
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery("SELECT * FROM " + tableName);
			while (results.next()) {
				StringBuilder str = new StringBuilder();
				str.append(results.getInt(1));
				str.append(". ");
				str.append(results.getString(2));
				str.append(":");
				str.append(results.getString(3));
				st[curInd++]=str.toString();
			}
		} catch (Exception e) {
			Server.l(e.getMessage());
		}
		return st;
	}

	private void shutdown() {
		try {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				DriverManager.getConnection(dbURL + ";shutdown=true");
				conn.close();
			}
		} catch (SQLException sqlExcept) {

		}

	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		shutdown();
	}

	

}
