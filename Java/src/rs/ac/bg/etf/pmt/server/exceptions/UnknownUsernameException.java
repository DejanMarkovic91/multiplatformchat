package rs.ac.bg.etf.pmt.server.exceptions;

public class UnknownUsernameException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Thrown if unknown username is specified as destination in file sending
	 * process
	 * 
	 * @param source_username
	 *            Username of user that attempted illegal action
	 * @param target_username
	 *            Username that doesn't exist
	 */
	public UnknownUsernameException(String source_username,
			String target_username) {
		super("User " + source_username
				+ " tried to send file to unknown username: " + target_username);
	}

}
