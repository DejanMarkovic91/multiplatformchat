package rs.ac.bg.etf.pmt.server;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class Messager extends Handler {

	private String name;
	private String message;
	private String address;
	private Connection connection;

	public Messager(Server server, String name, String message, String address,
			Connection connection) {
		super(server);
		this.name = name;
		this.message = message;
		this.connection = connection;
	}

	@Override
	public void handleRequest() {
		try{
		if (name.equals("Unknown")) {
			String tmp = new StringTokenizer(message, ":").nextToken();
			if (('/' + tmp) != address && tmp != address)
				name = tmp;
		}
		StringTokenizer tmp = new StringTokenizer(message, ":");
		tmp.nextToken();
		String data = tmp.nextToken();
		if (!data.equals(""))
			server.sendToAll(name + ":" + data + "\n", connection);
		}catch(NoSuchElementException e){
			
		}
	}

}
