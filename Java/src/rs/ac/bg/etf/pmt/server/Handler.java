package rs.ac.bg.etf.pmt.server;

public abstract class Handler {

	protected Server server;

	public Handler(Server server) {
		this.server = server;
	}
	
	public abstract void handleRequest();

}
