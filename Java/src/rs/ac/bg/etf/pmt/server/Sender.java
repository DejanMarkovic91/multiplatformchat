package rs.ac.bg.etf.pmt.server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener implementation for sending message
 * 
 */

class Sender implements ActionListener {

	private Server s;

	Sender(Server s) {
		this.s = s;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (s.box.getText().toString().equals(""))
			return;
		String data = s.Name + ": " + s.box.getText().toString();
		s.area.append(data + "\n");
		s.sendToAll(data, null);
		s.box.setText("");
	}

}