package rs.ac.bg.etf.pmt.server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * ActionListener implementation for exiting application
 * 
 */
class Exiter implements ActionListener {

	private Server s;

	Exiter(Server s) {
		this.s = s;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			s.disconnect();
		} catch (IOException ex) {
		}
		s.dispose();
	}

}
