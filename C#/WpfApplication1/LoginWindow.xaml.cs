﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private MainWindow mainw;

        public LoginWindow(MainWindow mw)
        {
            InitializeComponent();
            mainw = mw;
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e)
        {
            if (mainw == null || !mainw.Connected)
            {
                MessageBox.Show("ERROR: Not connected to server!");
            }
            else 
            {
                byte[] msg = Encoding.ASCII.GetBytes("#LOGIN#" + tbxUsername.Text + "#" + tbxPassword.Text + "\n");
                int bytesSend = mainw.SenderSocket.Send(msg);
                mainw.VUsername = tbxUsername.Text;
            }
            Close();
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                byte[] msg = Encoding.ASCII.GetBytes("#LOGIN#" + tbxUsername.Text + "#" + tbxPassword.Text + "\n");
                int bytesSend = mainw.SenderSocket.Send(msg);
                Close();
            }
        }
    }
}
