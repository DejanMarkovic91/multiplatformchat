﻿using System;
using System.Text;
using System.Windows;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using Microsoft.VisualBasic;

namespace WpfApplication1
{
    public partial class MainWindow : Window
    {

        // Receiving byte array  
        byte[] bytes = new byte[1024];
        Socket senderSock;
        bool connected = false;
        string username = System.Environment.UserName;
        string validationUsername = string.Empty;

        public bool Connected 
        {
            get { return connected; }
        }

        public string VUsername
        {
            set { validationUsername = value; }
        }

        public Socket SenderSocket
        {
            get { return senderSock; }
        }

        public MainWindow()
        {
            InitializeComponent();

            Send_Button.IsEnabled = true;
            Disconnect_Button.IsEnabled = true;
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (connected)
                {
                    MessageBox.Show("INFO: Already connected!");
                    return;
                }
                // Create one SocketPermission for socket access restrictions 
                SocketPermission permission = new SocketPermission(
                    NetworkAccess.Connect,    // Connection permission 
                    TransportType.Tcp,        // Defines transport types 
                    "",                       // Gets the IP addresses 
                    SocketPermission.AllPorts // All ports 
                    );

                // Ensures the code to have permission to access a Socket 
                permission.Demand();

                // Resolves a host name to an IPHostEntry instance            
                IPHostEntry ipHost = Dns.GetHostEntry("");




                // Gets first IP address associated with a localhost 

                IPAddress ipAddr = null;

                if (tbMsg_Copy.Text.Equals("localhost")) { ipAddr = ipHost.AddressList[0]; }
                else
                    ipAddr = IPAddress.Parse((tbMsg_Copy.Text));//ipHost.AddressList[0];


                // Creates a network endpoint 
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 16000);

                // Create one Socket object to setup Tcp connection 
                senderSock = new Socket(
                    ipAddr.AddressFamily,// Specifies the addressing scheme 
                    SocketType.Stream,   // The type of socket  
                    ProtocolType.Tcp     // Specifies the protocols  
                    );

                senderSock.NoDelay = false;

                // Establishes a connection to a remote host 
                senderSock.Connect(ipEndPoint);
                tbStatus.Text = "Socket connected to " + senderSock.RemoteEndPoint.ToString();
                username = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();
                username = username.Replace(":",".");

                byte[] msgSend = Encoding.ASCII.GetBytes("#connect request#\n");
                byte[] msgReceive = new byte[100];
                senderSock.Send(msgSend);
                senderSock.Receive(msgReceive);

                String receivedMessage = System.Text.Encoding.ASCII.GetString(msgReceive);
                if (receivedMessage.CompareTo("#connected#") != 0) {
                    tbStatus.Text = "Could not connect to specified server!";
                }

                mainw = this;
                tbStatus.Text = "Socket connected to " + senderSock.RemoteEndPoint.ToString();
                connected = true;
                Thread handler = new Thread(new ThreadStart(runThread));

                handler.Start();
                Disconnect_Button.IsEnabled = true;
                Connect_Button.IsEnabled = false;
                Send_Button.IsEnabled = true;
            }
            catch (Exception exc) { MessageBox.Show(exc.Message, "Error"); }

        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            try
            {
                byte[] msgSend = Encoding.ASCII.GetBytes("#END#\n");
                senderSock.Send(msgSend);
                senderSock.Shutdown(SocketShutdown.Both);

                //Closes the Socket connection and releases all resources 
                senderSock.Close();
            }
            catch (Exception) { }
            Disconnect_Button.IsEnabled = false;

        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string theMessageToSend = tbMsg.Text;
                if ((theMessageToSend.CompareTo(string.Empty) == 0)) {
                    MessageBox.Show("Please enter proper message!");
                    return;
                }
                tbReceivedMsg.Text += username + ":" + tbMsg.Text +"\n";
                tbMsg.Text = "";
                byte[] msg = Encoding.ASCII.GetBytes(username + ":" + theMessageToSend + "\n");
                int bytesSend = senderSock.Send(msg);
                tbReceivedMsg.ScrollToEnd();
                Send_Button.IsEnabled = true;
                Disconnect_Button.IsEnabled = true;
            }
            catch (Exception) {}
        }


        static MainWindow mainw;
        public static void runThread()
        {
            byte[] buffer = new byte[100];
            while (mainw.connected)
            {
                for (int i = 0; i < 100; i++)
                {
                    buffer[i] = 0;

                }
                Thread.Sleep(1000);
                try
                {
                    int size = mainw.senderSock.Receive(buffer);
                    string data = Encoding.ASCII.GetString(buffer, 0, size);

                    if (data.CompareTo("#AUTHENTICATED#\n") == 0)
                    {
                        mainw.username = mainw.validationUsername;
                        MainWindow.mainw.Dispatcher.Invoke((Action)(() =>
                        {
                            mainw.updateText("Successfully authenticated!\n", mainw.tbReceivedMsg);
                        }));
                    }
                    else if (data.CompareTo("#RECEIVE_FILE#\n") == 0)
                    {

                        size = mainw.senderSock.Receive(buffer);
                        data = Encoding.ASCII.GetString(buffer, 0, size);

                        string[] words = data.Split('#');
                        string filename = words[1];
                        int filesize = Convert.ToInt32(words[2]);
                        string sender_username = words[3];
                        
                        MainWindow.mainw.Dispatcher.Invoke((Action)(() =>
                        {
                            mainw.updateText("User " + sender_username 
                                + " attempting to send file "+filename+"!", mainw.tbReceivedMsg);
                        }));

                        FileStream file = File.Create(filename);

                        int current_size = 0;
                        while (current_size < filesize) 
                        {
                            size = mainw.senderSock.Receive(buffer);
                            file.Write(buffer, 0, size);
                            current_size += size;
                        }

                        file.Flush();
                        file.Close();

                        MessageBox.Show("INFO: File: " + filename + " received from: " + sender_username);

                    }
                    else
                    {
                        if (data[data.Length - 1] == '\n') 
                        {
                            data = data.Substring(0, data.Length - 1);
                        }
                    }
                }
                catch (Exception e) {
                    MessageBox.Show(e.Message);
                }

            }
        }




        public void updateText(String sss, System.Windows.Controls.TextBox tb)
        {

            tb.Text += sss;
            tb.ScrollToEnd();

        }

        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Disables sends and receives on a Socket.
                connected = false;
                byte[] msgSend = Encoding.ASCII.GetBytes("#END#\n");
                senderSock.Send(msgSend);
                senderSock.Shutdown(SocketShutdown.Both);

                //Closes the Socket connection and releases all resources 
                senderSock.Close();

                Disconnect_Button.IsEnabled = false;
                Connect_Button.IsEnabled = true;
                tbStatus.Text = "Socket disconected";
            }
            catch (Exception) { }
        }

        private void tbMsg_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Send_Click(null, null);
            }
        }

        private void tbMsg_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tbMsg.Text.CompareTo("Your message here.") == 0)
            {
                tbMsg.Text = string.Empty;
            }
        }

        private void Login_button_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow secondWindow = new LoginWindow(this);
            secondWindow.Show();
        }

        ~MainWindow() 
        {
            Disconnect_Click(null, null);
        }

        private void tbReceivedMsg_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            tbReceivedMsg.ScrollToEnd();
        }

        private void tbMsg_PreviewDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void tbMsg_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        private void tbMsg_PreviewDrop(object sender, DragEventArgs e)
        {
            // Get data object
            var dataObject = e.Data as DataObject;

            // Check for file list
            if (dataObject.ContainsFileDropList())
            {
                // Process file names
                StringCollection fileNames = dataObject.GetFileDropList();

                

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FileSendWindow secondWindow = new FileSendWindow(this);
            secondWindow.Show();
        }
    }
}
