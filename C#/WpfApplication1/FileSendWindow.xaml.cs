﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.IO;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for FileSendWindow.xaml
    /// </summary>
    public partial class FileSendWindow : Window
    {
        private MainWindow mainw;
        private string filename = string.Empty;

        public FileSendWindow(MainWindow main)
        {
            InitializeComponent();
            mainw = main;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //open file chooser
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                // Open document 
                filename = dlg.FileName;
                info_box.Content = "File choosen: " + dlg.FileName;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!tb_username.Text.Equals(string.Empty) && !filename.Equals(string.Empty)) 
            {
                if (mainw.Connected)
                {
                    try
                    {
                        mainw.Dispatcher.Invoke((Action)(() =>
                        {
                            mainw.updateText("Attempting to send file: " + filename + "\n", mainw.tbReceivedMsg);
                        }));


                        if (tb_username.Text.CompareTo(String.Empty) == 0)
                        {
                            return;
                        }

                        string name = Path.GetFileName(filename);
                        long filesize = new System.IO.FileInfo(filename).Length;
                        byte[] msgSend = Encoding.ASCII.GetBytes("#SEND_FILE#\n" + name + "\n" + filesize + "\n" 
                            + tb_username.Text + "\n");
                        mainw.SenderSocket.Send(msgSend);
                        mainw.SenderSocket.SendFile(filename);
                        Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex);
                    }

                }
                else
                {
                    MessageBox.Show("ERROR: Can't send file if you are not connected!");
                }
            }
            else
            {
                MessageBox.Show("ERROR: Please supply username to send and|or file to send!");
            }
        }
    }
}
