package rs.ac.bg.etf.chat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button send_btn, connect_btn, disconnect_btn;
	static TextView area;
	EditText msg_text, ipaddr_text;
	protected static AtomicBoolean isConnected = new AtomicBoolean(false);
	static Socket socket = null;
	static BufferedReader in;
	static PrintWriter out;
	protected static IncommingStreamListener listener;
	protected static String myName = "";
	protected static String currLoginName = "";
	protected static String area_content = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		send_btn = (Button) findViewById(R.id.send_btn);
		connect_btn = (Button) findViewById(R.id.connect_btn);
		disconnect_btn = (Button) findViewById(R.id.disconnect_btn);
		area = (TextView) findViewById(R.id.text_area);
		msg_text = (EditText) findViewById(R.id.msg_text);
		ipaddr_text = (EditText) findViewById(R.id.IPAddress_fld);
		area.setEnabled(false);
		area.setTextColor(Color.CYAN);
		area.setBackgroundColor(Color.GRAY);
		area.setText(area_content);

		send_btn.setOnClickListener(new SenderListener());
		connect_btn.setOnClickListener(new Connecter());
		disconnect_btn.setOnClickListener(new Disonnecter());
	}

	@Override
	public void onPause() {
		super.onPause();
		area_content = area.getText().toString();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.exit:
			finish();
			return true;
		case R.id.send_file:
			if (isConnected.get()) {
				Intent intent = new Intent(this, SendFileActivity.class);
				startActivity(intent);
			} else {
				Toast.makeText(
						this,
						"You can't send file if you are not connected to server!",
						Toast.LENGTH_SHORT).show();
			}
			return true;
		case R.id.help:
			showHelpDialog();
			return true;
		case R.id.login:
			showLoginDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void showHelpDialog() {
		LayoutInflater inflater = getLayoutInflater();
		View help_view = inflater.inflate(R.layout.help, null);
		final TextView area = (TextView) help_view
				.findViewById(R.id.dialog_text_area);
		InputStream input = getResources().openRawResource(R.raw.help);
		BufferedReader br = new BufferedReader(new InputStreamReader(input));
		String readLine = null;
		StringBuilder text = new StringBuilder();
		try {
			while ((readLine = br.readLine()) != null) {
				text.append(readLine);
				text.append("\n");
			}
		} catch (IOException e) {
		} finally {
			try {
				if (br != null)
					br.close();
				if (in != null)
					in.close();
			} catch (IOException e) {
			}
		}
		area.setText(text);
		Linkify.addLinks(area, Linkify.ALL);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.help_text).setView(help_view);
		builder.setPositiveButton(android.R.string.ok, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	protected void showLoginDialog() {
		LayoutInflater inflater = getLayoutInflater();
		View login_view = inflater.inflate(R.layout.login, null);
		final EditText username = (EditText) login_view
				.findViewById(R.id.username_fld);
		final EditText password = (EditText) login_view
				.findViewById(R.id.password_fld);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.login_text).setView(login_view);
		builder.setNegativeButton(android.R.string.cancel,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}

				});
		builder.setPositiveButton(android.R.string.ok, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				String user = username.getText().toString();
				String pass = password.getText().toString();
				if (user == "" || pass == "") {
					Toast.makeText(MainActivity.this,
							R.string.credential_error, Toast.LENGTH_SHORT)
							.show();
					dialog.cancel();
				}
				currLoginName = user;
				String msg = "#LOGIN#" + user + "#" + pass + "#\n";
				out.print(msg);
				out.flush();
			}

		});
		builder.show();
	}

	class Connecter implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			if (isConnected.get()) {
				Toast.makeText(MainActivity.this,
						R.string.already_connected_info, Toast.LENGTH_SHORT)
						.show();
			} else {
				Thread thread = new Thread() {
					@Override
					public void run() {
						String server = ipaddr_text.getText().toString();
						if (server == "") {
							return;
						}
						Socket s = null;
						try {
							s = new Socket(server, 16000);
							in = new BufferedReader(new InputStreamReader(
									s.getInputStream()));
							out = new PrintWriter(s.getOutputStream(), true);
							out.print("#connect request#\n");
							out.flush();
							String retmsg = in.readLine();
							StringTokenizer str = new StringTokenizer(retmsg,
									"#");
							String tmp = str.nextToken();
							if (!tmp.equals("connected")) {
								throw new Exception();
							}
							socket = s;
							listener = new IncommingStreamListener();
							myName = socket.getLocalAddress().toString()
									.substring(1);
							listener.start();
							isConnected.set(true);
							MainActivity.this.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									Toast.makeText(MainActivity.this,
											R.string.connect_succ_info,
											Toast.LENGTH_SHORT).show();
								}

							});
						} catch (Exception e) {
							e.printStackTrace();
							MainActivity.this.runOnUiThread((new Runnable() {
								public void run() {
									Toast.makeText(MainActivity.this,
											R.string.connect_error,
											Toast.LENGTH_SHORT).show();
								}
							}));
							return;
						}
					}
				};
				thread.start();
			}
		}

	}

	class Disonnecter implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			disconnect();
			Toast.makeText(MainActivity.this, R.string.disconnect_info,
					Toast.LENGTH_SHORT).show();
		}

	}

	class IncommingStreamListener extends Thread {

		@Override
		public void run() {
			try {
				while (isConnected.get()) {
					final String msg = in.readLine();
					if (msg == null)
						continue;
					StringTokenizer tkn = new StringTokenizer(msg);
					String first = tkn.nextToken();
					if (msg.equals("#AUTHENTICATED#")) {
						MainActivity.this.runOnUiThread((new Runnable() {
							public void run() {
								area.append("Successfully logged in!\n");
								area.invalidate();
							}
						}));
						myName = currLoginName;
					} else if (msg.equals("#RECEIVE_FILE#")) {
						String datain = in.readLine();
						StringTokenizer t = new StringTokenizer(datain, "#");
						final String filename = t.nextToken();
						int filesize = Integer.parseInt(t.nextToken());
						final String sender = t.nextToken();

						int len = 0, curr_size = 0;
						byte[] buffer = new byte[filesize];
						InputStream input = socket.getInputStream();
						File file = new File(Environment
								.getExternalStorageDirectory()
								.getAbsolutePath()
								+ "//" + filename);
						System.out.println(filename + " " +filesize);
						FileOutputStream fout = new FileOutputStream(file);

						while (curr_size < filesize
								&& (len = input.read(buffer)) != -1) {
							fout.write(buffer, 0, len);
							curr_size += len;
						}
						
						MainActivity.this.runOnUiThread((new Runnable() {
							public void run() {
								area.append("File " + filename
										+ " is being received!\n");
								area.invalidate();
							}
						}));
						fout.flush();
						fout.close();

					} else {
						MainActivity.this.runOnUiThread((new Runnable() {
							public void run() {
								area.append(msg + "\n");
								area.invalidate();
							}
						}));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class SenderListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			if (isConnected.get()) {
				String txt = msg_text.getText().toString();
				System.err.println(txt);
				if (txt.equals(""))
					return;
				final String tmp = myName + ": " + txt + "\n";

				if (tmp != null)
					MainActivity.this.runOnUiThread((new Runnable() {
						public void run() {
							area.append(tmp);
							area.invalidate();
							msg_text.setText("");
						}
					}));

				out.print(tmp);
				out.flush();
			} else {
				MainActivity.this.runOnUiThread((new Runnable() {
					public void run() {
						Toast.makeText(MainActivity.this,
								R.string.not_connected_error,
								Toast.LENGTH_SHORT).show();
					}
				}));
			}
		}
	}

	/**
	 * Disconnects Client on android device from Server
	 */
	private void disconnect() {
		try {
			if (out != null)
				out.close();
			if (in != null)
				in.close();
			if (socket != null)
				socket.close();
		} catch (Exception e) {
		}

		isConnected.set(false);
	}

	/**
	 * Used for exiting app when such option is chosen in options menu
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		/*
		 * if (out != null) { out.print("#END#\n"); out.flush(); }
		 */
		// disconnect();
	}

}
