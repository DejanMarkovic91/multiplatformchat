package rs.ac.bg.etf.chat;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class FileChooserActivity extends Activity {

	public static final String FILE_PATH = "FILE_PATH";

	private int level = 0;
	private File root;
	private File current_level;
	private ListView files;
	private Button submit;
	private Button folder_up;
	private String selected_file;
	private boolean status = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_chooser);

		root = Environment.getExternalStorageDirectory();
		current_level = root;

		System.err.println(root.getName());

		files = (ListView) findViewById(R.id.file_list);
		submit = (Button) findViewById(R.id.select_file);
		folder_up = (Button) findViewById(R.id.folder_up);

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (selected_file != null && selected_file != "") {
					Intent intent = new Intent();
					intent.putExtra(FILE_PATH, selected_file);
					setResult(RESULT_OK, intent);
					status = true;
					finish();
				}
			}
		});

		folder_up.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (level > 0) {
					level--;
					current_level = current_level.getParentFile();

					files.setAdapter(new ArrayAdapter<String>(
							FileChooserActivity.this,
							android.R.layout.simple_list_item_1, current_level
									.list()));
				}
			}
		});

		files.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, root.list()));

		files.setOnItemClickListener(new FileItemListener(this));

	}

	@Override
	public void onPause() {
		super.onPause();
		if (!status) {
			Intent intent = new Intent();
			setResult(RESULT_CANCELED, intent);
		}
	}

	public File getCurrentDirectory() {
		return current_level;
	}

	public void setCurrentDirectory(File levelf) {
		current_level = levelf;
		files.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, levelf.list()));
		level++;
	}

	public void setCurrentlySelectedFile(String name) {
		selected_file = name;
	}

}
