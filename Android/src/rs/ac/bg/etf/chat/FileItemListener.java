package rs.ac.bg.etf.chat;

import java.io.File;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class FileItemListener implements OnItemClickListener {

	private FileChooserActivity activity;

	public FileItemListener(FileChooserActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		File selected_file = activity.getCurrentDirectory().listFiles()[position];

		if (selected_file.isDirectory()) {
			activity.setCurrentDirectory(selected_file);
		} else {
			activity.setCurrentlySelectedFile(selected_file.getAbsolutePath());
		}

	}

}
