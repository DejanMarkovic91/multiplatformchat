package rs.ac.bg.etf.chat;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SendFileActivity extends Activity {

	private static final int REQUEST_CODE = 1;

	private EditText username_to_send;
	private Button find_file;
	private Button send_file;
	private TextView status;
	private String file_path = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_file);

		username_to_send = (EditText) findViewById(R.id.username_to_send);
		find_file = (Button) findViewById(R.id.open_file_chooser);
		send_file = (Button) findViewById(R.id.send_file);
		status = (TextView) findViewById(R.id.sending_status);

		addListeners();

	}

	private void addListeners() {
		find_file.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SendFileActivity.this,
						FileChooserActivity.class);
				startActivityForResult(intent, REQUEST_CODE);
			}
		});

		send_file.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (username_to_send.getText().toString() != "") {
					try {
						File file = new File(file_path);
						FileInputStream in = new FileInputStream(file);
						OutputStream out = MainActivity.socket.getOutputStream();

						MainActivity.out.write("#SEND_FILE#\n");
						MainActivity.out.write(file.getName() + "\n" + file.length()
								+ "\n" + username_to_send.getText().toString() + "\n");
						MainActivity.out.flush();

						int size = (int) file.length();
						byte[] buffer = new byte[size];

						int len = 0, curr_size = 0;
						while (curr_size < size
								&& (len = in.read(buffer)) != -1) {
							out.write(buffer, 0, len);
							curr_size += len;
						}
						out.flush();
						in.close();
						finish();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					status.setText("Please supply username to which to send file!");
				}
			};
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check which request we're responding to
		if (requestCode == REQUEST_CODE) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				file_path = data.getStringExtra(FileChooserActivity.FILE_PATH);
				send_file.setEnabled(true);
				status.setText("Selected file:" + file_path);
			} else {
				// do nothing
			}
		}
	}

}
