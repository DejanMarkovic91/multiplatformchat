from tkinter import *
from tkinter import messagebox
from threading import Thread
from tkinter import filedialog
import time
import socket
import webbrowser

status = 0
new = 2
comPort = 0
msg = "Dejan"
name = socket.gethostbyname(socket.gethostname())
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
entU = NONE
entP = NONE
thread = 0
varname = ""
sender_username=""
connected = False
threadState = True
serverIPString = 'localhost'
tmpName = ""
global toplevel

class takeInput(object):

    def __init__(self,requestMessage):
        self.root = Tk()
        self.string = ''
        self.frame = Frame(self.root)
        self.frame.pack()        
        self.acceptInput(requestMessage)

    def acceptInput(self,requestMessage):
        r = self.frame

        k = Label(r,text=requestMessage)
        k.pack(side='left')
        self.e = Entry(r,text='Username')
        self.e.pack(side='left')
        self.e.focus_set()
        b = Button(r,text='Submit',command=self.gettext)
        b.pack(side='right')

    def gettext(self):
        self.string = self.e.get()
        self.root.destroy()
        self.root.quit()

    def getString(self):
        return self.string

    def waitForInput(self):
        self.root.mainloop()

def getText(requestMessage):
    msgBox = takeInput(requestMessage)
    #loop until the user makes a decision and the window is destroyed
    msgBox.waitForInput()
    return msgBox.getString()

def main():
    global textArea
    global btnSendMsg
    global btnConnect
    global btnDisconnect
    global msg
    global mGui
    mGui = Tk()
    username = StringVar()
    password = StringVar()
    varname = StringVar()
    msg = StringVar()
    serverIPString = StringVar()
    mGui.geometry("400x500+200+20")
    mGui.title("Chat client v1.0")
    mGui.bind("<Return>", execute)
    frame1 = Frame(mGui)
    labServer = Label(frame1, text="Server IP adress: ").grid(row=0, column=0)
    entServer = Entry(frame1, textvariable=serverIPString)
    entServer.insert(0, "localhost")
    entServer.grid(row=0, column=1)
    btnConnect = Button(frame1, text="Connect", fg="red", bg="blue", command=Connect)
    btnConnect.grid(row=1, column=0)
    btnDisconnect = Button(frame1, text="Disconnect", fg="red", bg="blue", command=Disconnect)
    btnDisconnect.config(state=DISABLED)
    btnDisconnect.grid(row=1, column=1)
    frame1.pack()
    frmArea = Frame(mGui)
    yscroll = Scrollbar(frmArea)
    yscroll.pack(side=RIGHT, fill=Y)
    textArea = Text(frmArea, bg="yellow", yscrollcommand=yscroll.set)
    textArea.pack()
    frmArea.pack()
    textArea.config(state=DISABLED)
    frame2 = Frame(mGui)
    btnSendMsg = Button(frame2, text="Send message", fg="white", bg="black", command=sendMsg).grid(row=0, column=0)
    entMessage = Entry(frame2, textvariable=msg)
    entMessage.grid(row=0, column=1)
    frame2.pack()

    menuBar = Menu(mGui)

    fileMenu = Menu(menuBar, tearoff=0)
    fileMenu.add_command(label="Exit", command=ExitApplication)
    menuBar.add_cascade(label="File", menu=fileMenu)

    optionsMenu = Menu(menuBar, tearoff=0)
    optionsMenu.add_command(label="Log in", command=changeName)
    optionsMenu.add_separator()
    optionsMenu.add_command(label="Connect", command=Connect)
    optionsMenu.add_command(label="Disconnect", command=Disconnect)
    optionsMenu.add_command(label="Send file to user", command=SendFile)
    menuBar.add_cascade(label="Options", menu=optionsMenu)

    helpMenu = Menu(menuBar, tearoff=0)
    helpMenu.add_command(label="Help", command=helpCommand)
    helpMenu.add_command(label="About", command=aboutCommand)
    menuBar.add_cascade(label="Info", menu=helpMenu)
    mGui.config(menu=menuBar)

    mGui.protocol("VM_DELETE_WINDOW", ExitApplication)

    mGui.mainloop()

def login():
    global sock
    global textArea
    global connected
    global tmpName
    global entU
    global entP
    if(connected == False):
        messagebox.showerror("Error", "Not connected to server!")
        return
    if(entU.get() == "" or entP.get() == ""):
        print("no username and/or password\n")
        return
    message = "#LOGIN#" + entU.get() + "#" + entP.get() + "#\n"
    tmpName = entU.get()
    sock.send(bytes(message, "UTF-8"))
    toplevel.destroy()
    toplevel.quit()

def changeName():
    global name
    global toplevel
    global entU
    global entP
    toplevel = Tk()
    toplevel.title("Log in")
    mesg = Label(toplevel, text="Enter username and password.")
    mesg.pack()
    frameS = Frame(toplevel)
    Label(frameS, text="Username: ").grid(row=0, column=0)
    entU = Entry(frameS)
    entU.grid(row=0, column=1)
    Label(frameS, text="Password: ").grid(row=1, column=0)
    entP = Entry(frameS, show="*")
    entP.grid(row=1, column=1)
    frameS.pack()
    b1 = Button(toplevel, text="Submit", command=login)
    b1.pack()
    toplevel.mainloop()

def threadFunc():
    global sock
    global textArea
    global status
    global connected
    global threadState
    while(threadState):
        data = ""
        while(data == "" and connected == True and status == 0):
            try:
                answer = sock.recvfrom(256)
                data = answer[0].decode("utf-8")
            except:
                data = ""
                time.sleep(1)
        if(data == "#RECEIVE_FILE#\n"):
            status = 1
            data_rcvd = sock.recvfrom(256)
            data = data_rcvd[0].decode('UTF-8')
            tokens = data.split('#')
            filename = tokens[1]
            print(tokens[2])
            filesize = int(tokens[2])
            sender_username = tokens[3]
            text_file = open(filename, "wb")
            messagebox.showwarning("File receivinig", "File is being received from " + sender_username + "!")
            current_size = 0
            
            while(current_size<filesize):
                data_rcvd = sock.recvfrom(filesize)
                text_file.write(data_rcvd[0])
                current_size = current_size + len(data_rcvd[0])
                
            text_file.close()
            status = 0
        else :
            if(data == "#DROP CONNECTION#\n"):Disconnect()
            if(data == "#AUTHENTICATED#\n"):authenticated()
            else:
                try:
                    textArea.config(state=NORMAL)
                    if(data.endswith('\n')):
                        data = data[:-1]
                    textArea.insert(END, data)
                    textArea.see(END)
                    textArea.config(state=DISABLED)
                except TclError:
                    print("Error on gui!\n");
                    return

def authenticated():
    global name
    global tmpName
    global textArea
    textArea.config(state=NORMAL)
    textArea.insert(END, "You successfully logged in!\n")
    name = tmpName
    textArea.see(END)
    textArea.config(state=DISABLED)

def sendMsg():
    global sock
    global textArea
    global connected
    global msg
    if(connected == False):
        messagebox.showerror("Error", "Not connected to server!")
        return
    if(msg.get() == ""):
        return
    message = name + ": " + msg.get() + "\n"
    textArea.config(state=NORMAL)
    textArea.insert(END, message)
    textArea.see(END)
    textArea.config(state=DISABLED)
    msg.set("")
    if(connected == True):sock.send(bytes(message, 'UTF-8'))

def aboutCommand():
    messagebox.showinfo("Help", "Simple web cliebnt made for java server by Dejan Markovic.\n" +
                        "There are also versions of this app in Android and C#."+ 
        "If you have any suggestions  you can contact me on bodemarkovic@gmail.com.\n\nHappy using!")

def helpCommand():
    url = "http://rti.etf.bg.ac.rs"
    webbrowser.open(url, new=new)

def Connect():
    global sock
    global thread
    global connected
    global textArea
    global btnConnect
    global btnDisconnect
    if (connected):
        return;
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((serverIPString, 16000))
    except OSError:
        messagebox.showerror("Error", "Eather you entered invalid IP address or the server is down!")
        return
    sock.send(bytes("#connect request#\n", 'UTF-8'))
    reply = sock.recvfrom(256)
    answer = reply[0].decode("utf-8")
    answer = answer.split('#', 3)[1]
    if(answer != "connected"):
        messagebox.showerror("Error", "Connection failed!")
        return
    thread = Thread(target=threadFunc)
    thread.start()
    connected = True
    textArea.config(state=NORMAL)
    textArea.insert(END, '***CONNECTED TO SERVER***\n')
    textArea.see(END)
    textArea.config(state=DISABLED)
    btnDisconnect.config(state=NORMAL)
    btnConnect.config(state=DISABLED)

def Disconnect():
    global sock
    global connected
    global btnConnect
    global btnDisconnect
    global threadState
    threadState = False
    if(connected):
        sock.send(bytes("#END#\n", 'UTF-8'))
        textArea.config(state=NORMAL)
        textArea.insert(END, "***DISCONNECTED FROM SERVER***\n")
        textArea.see(END)
        textArea.config(state=DISABLED)
    sock.close()
    connected = False
    btnDisconnect.config(state=DISABLED)
    btnConnect.config(state=NORMAL)
    
def SendFile():
    global connected
    global sock
    if connected:
        username = getText("User name to which you are sending file?")
        filename = filedialog.askopenfilename()
        file_object = open(filename, 'r', encoding='ISO-8859-1') 
        data = file_object.read()
        tokens = filename.split('/')
        filename = tokens[len(tokens)-1]
        if len(data)>10 * 1024 * 1024:
            messagebox.showerror("Error", "File too large to send!")
            return
        sock.send(bytes("#SEND_FILE#\n", 'UTF-8'))
        sock.send(bytes(filename+"\n",'UTF-8'))
        sock.send(bytes(str(len(data))+"\n",'UTF-8'))
        sock.send(bytes(username+"\n",'UTF-8'))
        sock.send(bytes(data,'ISO-8859-1'))
    else:
        messagebox.showerror("Error", "First connect to server to send file!")
        
def ExitApplication():
    global mGui
    Disconnect()
    mGui.destroy()
    mGui.quit()
    return

def execute(event):
    sendMsg()

if __name__ == '__main__':
    main()  
