# README #

Server is written in Java and has everything you need to administrate connections. (Register credentials, edit credentials, ban users, control flow of data, etc.)

Clients are written in C#, Android and Python. C# part is written in MS Visual Studio 2014, .NET framework 4.5. Android is for versions 4.0.3 and up. Python is programmed for version 3.4.1. All clients communicate only with server, and every needed data is received from server.

Any suggestions talk to me on bodemarkovic@gmail.com